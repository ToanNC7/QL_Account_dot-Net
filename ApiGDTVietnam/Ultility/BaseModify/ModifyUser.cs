﻿
using ApiGDTVietnam.Ultility.BaseControllers;
using System;
using System.Linq;
using System.Web;

namespace ApiGDTVietnam.Ultility.BaseModify
{
    public class ModifyUser<T> : BaseController
        where T : class
    {
        public static T ModifySet(T t, string m = null)
        {
            var method = !string.IsNullOrEmpty(m) ? m : HttpContext.Current.Request.HttpMethod;
            if (method == "POST")
            {
                t.GetType().GetProperty("Created_By").SetValue(t, TenTaiKhoan);
                t.GetType().GetProperty("Created_Date").SetValue(t, DateTime.Now);
            }
            else if ((new string[] { "PUT", "PATCH" }).Contains(method))
            {
                t.GetType().GetProperty("Updated_By").SetValue(t, TenTaiKhoan);
                t.GetType().GetProperty("Updated_Date").SetValue(t, DateTime.Now);
            }
            else if (method == "DELETE")
            {
                t.GetType().GetProperty("Deleted_By").SetValue(t, TenTaiKhoan);
                t.GetType().GetProperty("Deleted_Date").SetValue(t, DateTime.Now);
            }

            return t;
        }
    }
}