﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiGDTVietnam.Ultility.BaseMethod
{
    public class AuditEntities
    {
        [NotMapped]
        public string DV_ID { get; set; }
        [NotMapped]
        public string Created_By { get; set; }
        [NotMapped]
        public DateTime Created_Date { get; set; }
        [NotMapped]
        public string Updated_By { get; set; }
        [NotMapped]
        public Nullable<DateTime> Updated_Date { get; set; }
    }
    public class BaseEntities : AuditEntities
    {
        public Guid ID { get; set; }

    }
    public class BaseEntitiesWithDelete : AuditEntitiesWithDelete
    {
        public Guid ID { get; set; }
    }
    public class AuditEntitiesWithDelete : AuditEntities
    {
        [NotMapped]
        public string Deleted_By { get; set; }
        [NotMapped]
        public Nullable<DateTime> Deleted_Date { get; set; }
    }
}