﻿using ApiGDTVietnam.Ultility.BaseMethod;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;

namespace ApiGDTVietnam.Ultility.BaseControllers
{
    [Authorize]
    public class BaseOdataGetWithDVController<TKey, TEntity, TContext> : BaseOdataGetController<TKey, TEntity, TContext>
        where TEntity : class
        where TContext : BaseContext
    {

        [EnableQuery(PageSize = 50)]
        [HttpGet]
        public override IQueryable<TEntity> Get()
        {
            //var queryString = this.Request.GetQueryNameValuePairs();
            return db.Set<TEntity>().Where(GenericMethod.CreatePredicate<TEntity>("DV_ID", BaseController.IdDonVi));
        }

        [EnableQuery]
        [HttpGet]
        public override SingleResult<TEntity> Get([FromODataUri] TKey key)
        {
            return SingleResult.Create(db.Set<TEntity>().Where(GenericMethod.CreatePredicate<TEntity>("DV_ID", BaseController.IdDonVi)).Where(GenericMethod.CreatePredicate<TEntity>(GenericMethod.GetPrimaryKey<TEntity>(db), key)));
        }
    }
}
