﻿using ApiGDTVietnam.Models.QuanTriHeThong;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace ApiGDTVietnam.Ultility.BaseControllers
{
    public class BaseController : ApiController
    {
        public static CurrentUserModel GetCurrentUser()
        {
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var _tenTaiKhoan = identity.FindFirst("TenTaiKhoan");

                if (_tenTaiKhoan == null)
                {
                    return null;
                }
                else
                {
                    var user = new CurrentUserModel();
                    user.tenTaiKhoan = _tenTaiKhoan.Value;
                    var id_don_vi = identity.FindFirst("DonVi_Id");
                    if (id_don_vi != null)
                    {
                        user.donVi_Id = Guid.Parse(id_don_vi.Value);
                    }
                    var ma_don_vi = identity.FindFirst("MaDonVi");
                    if (ma_don_vi != null)
                    {
                        user.maDonVi = ma_don_vi.Value;
                    }

                    return user;
                }
            }

            return null;
        }

        public static string IdDonVi
        {
            get
            {
                var user = GetCurrentUser();
                if (user != null)
                {
                    return user.donVi_Id.ToString();
                }
                return null;
            }
        }

        public static string MaDonVi
        {
            get
            {
                var user = GetCurrentUser();
                if (user != null)
                {
                    return user.maDonVi;
                }
                return null;
            }
        }

        public static string TenTaiKhoan
        {
            get
            {
                var user = GetCurrentUser();
                if (user != null)
                {
                    return user.tenTaiKhoan;
                }
                return null;
            }
        }
    }
}
