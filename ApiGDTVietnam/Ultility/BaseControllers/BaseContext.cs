﻿using ApiGDTVietnam.Ultility.BaseModify;
using ApiGDTVietnam.Ultility.BaseObject;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ApiGDTVietnam.Ultility.BaseControllers
{
    public class BaseContext : DbContext
    {
        private string v;

        public BaseContext(string v)
        {
            this.v = v;
        }

        public string UserName
        {
            get; private set;
        }
        public string DonVi
        {
            get; private set;
        }
        public override int SaveChanges()
        {

            try
            {
                SavingChanges();
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
              //  throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }
        public int SaveChangesWithCache(string className, bool isDonVi = false)
        {
            try
            {
                CacheWithEntity cache = SavingChanges(true, className, isDonVi);
                var a = base.SaveChanges();
                if (HttpContext.Current.Request.HttpMethod == "DELETE")
                {
                    DictionaryStore.Remove(cache.name, cache.value);
                }
                else
                {
                    DictionaryStore.Add(cache.name, cache.value);
                }
                return a;
            }
            catch
            {
                throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }
        public override async Task<int> SaveChangesAsync()
        {
            try
            {
                SavingChanges();
                return await base.SaveChangesAsync();
            }
            catch
            {
                throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }
        public async Task<int> SaveChangesAsyncWithCache()
        {
            try
            {
                CacheWithEntity cache = SavingChanges(true);
                var a = await base.SaveChangesAsync();
                if (HttpContext.Current.Request.HttpMethod == "DELETE")
                {
                    DictionaryStore.Remove(cache.name, cache.value);
                }
                else
                {
                    DictionaryStore.Add(cache.name, cache.value);
                }
                return a;
            }
            catch
            {
                throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                SavingChanges();
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch
            {
                throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }
        public async Task<int> SaveChangesAsyncWithCache(CancellationToken cancellationToken)
        {
            try
            {
                SavingChanges(true);
                CacheWithEntity cache = SavingChanges(true);
                var a = await base.SaveChangesAsync(cancellationToken);
                if (HttpContext.Current.Request.HttpMethod == "DELETE")
                {
                    DictionaryStore.Remove(cache.name, cache.value);
                }
                else
                {
                    DictionaryStore.Add(cache.name, cache.value);
                }
                return a;
            }
            catch
            {
                throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
            }
        }

        private CacheWithEntity SavingChanges(bool cache = false, string className = null, bool isDonVi = false)
        {

            CacheWithEntity model = new CacheWithEntity();
            var objects = this.ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Added ||
                    p.State == EntityState.Deleted ||
                    p.State == EntityState.Modified);
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            UserName = identity?.FindFirst("TenTaiKhoan")?.Value ?? (identity?.IsAuthenticated == true ? "Anonymous" : "Public");
            DonVi = identity?.FindFirst("DonVi_Id")?.Value ?? (identity?.IsAuthenticated == true ? "Anonymous" : "Public");


            ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;
            foreach (DbEntityEntry entry in objects
                .Where(p => p.State == EntityState.Added))
            {
                if (entry.Entity != null)
                {
                    //Lấy primary key
                    var a = entry.Entity.GetType();
                    string name = a.Name;
                    MethodInfo method = typeof(ObjectContext).GetMethod("CreateObjectSet", Type.EmptyTypes)
                    .MakeGenericMethod(a);
                    dynamic objectSet = method.Invoke(objectContext, null);

                    Guid guid = Guid.NewGuid();
                    //a.GetType().GetProperty("TrangThai")?.SetValue(a, true);
                    a.GetProperty("Created_By").SetValue(entry.Entity, UserName);
                    a.GetProperty("Created_Date").SetValue(entry.Entity, DateTime.Now);
                    a.GetProperty("Id").SetValue(entry.Entity, guid);
                    if (cache && name == className)
                    {
                        model.name = !isDonVi ? name : (name + DonVi);
                        CacheModel cacheModel = new CacheModel()
                        {
                            k = guid.ToString(),
                            v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                        };
                        model.value = cacheModel;
                    }
                }
            }

            foreach (DbEntityEntry entry in objects
                .Where(p => p.State == EntityState.Modified))
            {
                if (entry.Entity != null)
                {
                    var a = entry.Entity.GetType().BaseType;
                    string name = a.Name;
                    if ((entry.Property("Created_By") != null && entry.Property("Created_By").IsModified) ||
                        (entry.Property("Created_Date") != null && entry.Property("Created_Date").IsModified))
                    {
                        throw new Exception("Thao tác thất bại. Vui lòng liên hệ GDTVietNam để được hỗ trợ!");
                    }

                    MethodInfo method = typeof(ObjectContext).GetMethod("CreateObjectSet", Type.EmptyTypes)
                    .MakeGenericMethod(a);
                    dynamic objectSet = method.Invoke(objectContext, null);
                    IEnumerable<dynamic> keyMembers = objectSet.EntitySet.ElementType.KeyMembers;
                    string keyNames = keyMembers.Select(k => (string)k.Name).First();

                    if (HttpContext.Current.Request.HttpMethod == "DELETE")
                    {
                        a.GetProperty("Deleted_By").SetValue(entry.Entity, UserName);
                        a.GetProperty("Deleted_Date").SetValue(entry.Entity, DateTime.Now);
                        if (cache && name == className)
                        {
                            model.name = !isDonVi ? name : (name + DonVi);
                            CacheModel cacheModel = new CacheModel()
                            {
                                k = a.GetProperty(keyNames).GetValue(entry.Entity).ToString(),
                                v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                            };
                            model.value = cacheModel;
                            //DictionaryStore.Remove(name,model);
                        }
                    }
                    else
                    {

                        a.GetProperty("Updated_By").SetValue(entry.Entity, UserName);
                        a.GetProperty("Updated_Date").SetValue(entry.Entity, DateTime.Now);
                        if (cache && name == className)
                        {
                            model.name = !isDonVi ? name : (name + DonVi);
                            CacheModel cacheModel = new CacheModel()
                            {
                                k = a.GetProperty(keyNames).GetValue(entry.Entity).ToString(),
                                v = a.GetProperty("Name")?.GetValue(entry.Entity).ToString()
                            };
                            model.value = cacheModel;

                        }
                    }
                }
            }
            return model;
        }
    }
}
