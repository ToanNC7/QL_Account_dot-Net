﻿using AApiGDTVietnam.Ultility.BaseObject;
using ApiGDTVietnam.Ultility.BaseMethod;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using System.Web.Http.OData;

namespace ApiGDTVietnam.Ultility.BaseControllers
{
    [Authorize]
    public class BaseOdataWithCacheController<TKey, TEntity, TContext> : BaseOdataGetController<TKey, TEntity, TContext>
        where TEntity : class
        where TContext : BaseContext
    {

        [HttpPost]
        public IHttpActionResult Post(TEntity entity)
        {
            entity = CustomPost(entity);
            //entity.GetType().GetProperty(GetPKey()).SetValue(entity, Guid.NewGuid());
            //ModifyUser<TEntity>.ModifySet(entity);
            //var a = (TKey)entity.GetType().GetProperty(GetPKey()).GetValue(entity, null);
            entity.GetType().GetProperty("TrangThai").SetValue(entity, true);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            validationPost(entity);
            if (entity.GetType().GetProperty("MangNgayThangNam") != null)
            {
                DateTime tuNgay = (DateTime)entity.GetType().GetProperty("TuNgay").GetValue(entity, null);
                DateTime denNgay = (DateTime)entity.GetType().GetProperty("DenNgay").GetValue(entity, null);
                if (tuNgay > denNgay)
                    return BadRequest("Từ ngày phải nhỏ hơn đến ngày!");
                List<NgayThangNamModel> ngayThangNams = NgayThangNamModel.get_NgayThangNam(tuNgay, denNgay, false);
                entity.GetType().GetProperty("MangNgayThangNam").SetValue(entity, NgayThangNamModel.JsonStringify(ngayThangNams));
            }

            db.Set<TEntity>().Add(entity);

            try
            {
                db.SaveChangesWithCache(entity.GetType().Name);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TExists((TKey)entity.GetType().GetProperty(GenericMethod.GetPrimaryKey<TEntity>(db)).GetValue(entity, null)))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Created(entity);
        }

        [AcceptVerbs("PATCH", "MERGE")]
        [HttpPatch]
        public IHttpActionResult Patch([FromODataUri] TKey key, Delta<TEntity> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TEntity entity = db.Set<TEntity>().Find(key);
            entity = CustomPatch(entity);
            validationPatch(entity);
            if (entity == null)
            {
                return NotFound();
            }
            //ModifyUser<TEntity>.ModifySet(entity);

            patch.Patch(entity);

            try
            {
                db.SaveChangesWithCache(entity.GetType().BaseType.Name);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(entity);
        }
    }
}
