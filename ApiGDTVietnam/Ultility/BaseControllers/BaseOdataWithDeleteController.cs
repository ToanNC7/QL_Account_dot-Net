﻿using System.Net;
using System.Web.Http;
using System.Web.Http.OData;

namespace ApiGDTVietnam.Ultility.BaseControllers
{
 //   [Authorize]
    public class BaseOdataWithDeleteController<TKey, TEntity, TContext> : BaseOdataController<TKey, TEntity, TContext>
        where TEntity : class
        where TContext : BaseContext
    {


        [HttpDelete]
        public IHttpActionResult Delete([FromODataUri] TKey key)
        {
            TEntity entity = db.Set<TEntity>().Find(key);
            if (entity == null)
            {
                return NotFound();
            }
            validationDelete(entity);

            //ModifyUser<TEntity>.ModifySet(entity);
            entity.GetType().GetProperty("TrangThai").SetValue(entity, false);

            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}