﻿using ApiGDTVietnam.DataProvider.EF;
using Newtonsoft.Json;
using Swashbuckle.Application;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;

namespace ApiGDTVietnam
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore };
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            // Web API configuration and services
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();




            //test 
            builder.EntitySet<SYS_ROLETS>("Roles");
            builder.EntitySet<SYS_TaiKhoanTS>("TaiKhoans");
            builder.EntitySet<SYS_Nhanvien>("SYS_Nhanvien");
            builder.EntitySet<SYS_ChucVu>("SYS_ChucVu");
            builder.EntitySet<SYS_CTy>("SYS_CTy");
            builder.EntitySet<SYS_KinhNghiem>("SYS_KinhNghiem");
            builder.EntitySet<SYS_CV_NV>("SYS_CV_NV");


            foreach (var a in builder.EntitySets)
            {
                var b = a.EntityType.IgnoredProperties;
            }

            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("SwaggerUI", "", null, null, new RedirectHandler(SwaggerDocsConfig.DefaultRootUrlResolver, "swagger/ui/index"));

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(
            config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml"));

        }
    }
}
