﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiGDTVietnam.Models
{
    public  class NhanVienDto
    {
        //nhanvien
        public Guid? id { get; set; }

        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string GioiTinh { get; set; }
        public ICollection<ChucVuDto> ChucVuDtos { get; set; }
    }

    public class ChucVuDto
    {
        //chucvu
        public Guid? id_CV { get; set; }
        public string TenChucVu { get; set; }
        public ICollection<NhanVienDto> NhanVienDtos { get; set; }


    }
    public class NhanVienModel
    {
        public List<NhanVienDto> nhanVienDtos { get; set; }
    }
}