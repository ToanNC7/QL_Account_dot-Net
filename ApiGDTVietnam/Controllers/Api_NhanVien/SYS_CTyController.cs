﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.Api_NhanVien
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SYS_CTy>("SYS_CTy");
    builder.EntitySet<SYS_Nhanvien>("SYS_Nhanvien"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SYS_CTyController : ODataController
    {
        private DatModelEntities1 db = new DatModelEntities1();

        // GET: odata/SYS_CTy
        [EnableQuery]
        public IQueryable<SYS_CTy> GetSYS_CTy()
        {
            return db.SYS_CTy;
        }

        // GET: odata/SYS_CTy(5)
        [EnableQuery]
        public SingleResult<SYS_CTy> GetSYS_CTy([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_CTy.Where(sYS_CTy => sYS_CTy.Id == key));
        }

        // PUT: odata/SYS_CTy(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_CTy> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_CTy sYS_CTy = db.SYS_CTy.Find(key);
            if (sYS_CTy == null)
            {
                return NotFound();
            }

            patch.Put(sYS_CTy);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_CTyExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_CTy);
        }

        // POST: odata/SYS_CTy
        public IHttpActionResult Post(SYS_CTy sYS_CTy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_CTy.Add(sYS_CTy);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SYS_CTyExists(sYS_CTy.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sYS_CTy);
        }

        // PATCH: odata/SYS_CTy(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_CTy> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_CTy sYS_CTy = db.SYS_CTy.Find(key);
            if (sYS_CTy == null)
            {
                return NotFound();
            }

            patch.Patch(sYS_CTy);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_CTyExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_CTy);
        }

        // DELETE: odata/SYS_CTy(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_CTy sYS_CTy = db.SYS_CTy.Find(key);
            if (sYS_CTy == null)
            {
                return NotFound();
            }

            db.SYS_CTy.Remove(sYS_CTy);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SYS_CTy(5)/SYS_Nhanvien
        [EnableQuery]
        public IQueryable<SYS_Nhanvien> GetSYS_Nhanvien([FromODataUri] Guid key)
        {
            return db.SYS_CTy.Where(m => m.Id == key).SelectMany(m => m.SYS_Nhanvien);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYS_CTyExists(Guid key)
        {
            return db.SYS_CTy.Count(e => e.Id == key) > 0;
        }
    }
}
