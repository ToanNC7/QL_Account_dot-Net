﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.Api_NhanVien
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SYS_KinhNghiem>("SYS_KinhNghiem");
    builder.EntitySet<SYS_Nhanvien>("SYS_Nhanvien"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SYS_KinhNghiemController : ODataController
    {
        private DatModelEntities1 db = new DatModelEntities1();

        // GET: odata/SYS_KinhNghiem
        [EnableQuery]
        public IQueryable<SYS_KinhNghiem> GetSYS_KinhNghiem()
        {
            return db.SYS_KinhNghiem;
        }

        // GET: odata/SYS_KinhNghiem(5)
        [EnableQuery]
        public SingleResult<SYS_KinhNghiem> GetSYS_KinhNghiem([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_KinhNghiem.Where(sYS_KinhNghiem => sYS_KinhNghiem.Id == key));
        }

        // PUT: odata/SYS_KinhNghiem(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_KinhNghiem> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_KinhNghiem sYS_KinhNghiem = db.SYS_KinhNghiem.Find(key);
            if (sYS_KinhNghiem == null)
            {
                return NotFound();
            }

            patch.Put(sYS_KinhNghiem);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_KinhNghiemExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_KinhNghiem);
        }

        // POST: odata/SYS_KinhNghiem
        public IHttpActionResult Post(SYS_KinhNghiem sYS_KinhNghiem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_KinhNghiem.Add(sYS_KinhNghiem);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SYS_KinhNghiemExists(sYS_KinhNghiem.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sYS_KinhNghiem);
        }

        // PATCH: odata/SYS_KinhNghiem(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_KinhNghiem> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_KinhNghiem sYS_KinhNghiem = db.SYS_KinhNghiem.Find(key);
            if (sYS_KinhNghiem == null)
            {
                return NotFound();
            }

            patch.Patch(sYS_KinhNghiem);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_KinhNghiemExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_KinhNghiem);
        }

        // DELETE: odata/SYS_KinhNghiem(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_KinhNghiem sYS_KinhNghiem = db.SYS_KinhNghiem.Find(key);
            if (sYS_KinhNghiem == null)
            {
                return NotFound();
            }

            db.SYS_KinhNghiem.Remove(sYS_KinhNghiem);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SYS_KinhNghiem(5)/SYS_Nhanvien
        [EnableQuery]
        public IQueryable<SYS_Nhanvien> GetSYS_Nhanvien([FromODataUri] Guid key)
        {
            return db.SYS_KinhNghiem.Where(m => m.Id == key).SelectMany(m => m.SYS_Nhanvien);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYS_KinhNghiemExists(Guid key)
        {
            return db.SYS_KinhNghiem.Count(e => e.Id == key) > 0;
        }
    }
}
