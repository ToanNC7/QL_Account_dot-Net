﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.Api_NhanVien
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SYS_CV_NV>("SYS_CV_NV");
    builder.EntitySet<SYS_ChucVu>("SYS_ChucVu"); 
    builder.EntitySet<SYS_Nhanvien>("SYS_Nhanvien"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SYS_CV_NVController : ODataController
    {
        private DatModelEntities1 db = new DatModelEntities1();

        // GET: odata/SYS_CV_NV
        [EnableQuery]
        public IQueryable<SYS_CV_NV> GetSYS_CV_NV()
        {
            return db.SYS_CV_NV;
        }

        // GET: odata/SYS_CV_NV(5)
        [EnableQuery]
        public SingleResult<SYS_CV_NV> GetSYS_CV_NV([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_CV_NV.Where(sYS_CV_NV => sYS_CV_NV.Id == key));
        }

        // PUT: odata/SYS_CV_NV(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_CV_NV> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_CV_NV sYS_CV_NV = db.SYS_CV_NV.Find(key);
            if (sYS_CV_NV == null)
            {
                return NotFound();
            }

            patch.Put(sYS_CV_NV);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_CV_NVExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_CV_NV);
        }

        // POST: odata/SYS_CV_NV
        public IHttpActionResult Post(SYS_CV_NV sYS_CV_NV)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_CV_NV.Add(sYS_CV_NV);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SYS_CV_NVExists(sYS_CV_NV.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sYS_CV_NV);
        }

        // PATCH: odata/SYS_CV_NV(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_CV_NV> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_CV_NV sYS_CV_NV = db.SYS_CV_NV.Find(key);
            if (sYS_CV_NV == null)
            {
                return NotFound();
            }

            patch.Patch(sYS_CV_NV);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_CV_NVExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_CV_NV);
        }

        // DELETE: odata/SYS_CV_NV(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_CV_NV sYS_CV_NV = db.SYS_CV_NV.Find(key);
            if (sYS_CV_NV == null)
            {
                return NotFound();
            }

            db.SYS_CV_NV.Remove(sYS_CV_NV);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SYS_CV_NV(5)/SYS_ChucVu
        [EnableQuery]
        public SingleResult<SYS_ChucVu> GetSYS_ChucVu([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_CV_NV.Where(m => m.Id == key).Select(m => m.SYS_ChucVu));
        }

        // GET: odata/SYS_CV_NV(5)/SYS_Nhanvien
        [EnableQuery]
        public SingleResult<SYS_Nhanvien> GetSYS_Nhanvien([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_CV_NV.Where(m => m.Id == key).Select(m => m.SYS_Nhanvien));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYS_CV_NVExists(Guid key)
        {
            return db.SYS_CV_NV.Count(e => e.Id == key) > 0;
        }
    }
}
