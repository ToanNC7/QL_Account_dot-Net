﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;

namespace ApiGDTVietnam.Controllers.Api_NhanVien
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SYS_ChucVu>("SYS_ChucVu");
    builder.EntitySet<SYS_CV_NV>("SYS_CV_NV"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SYS_ChucVuController : ODataController
    {
        private DatModelEntities1 db = new DatModelEntities1();

        // GET: odata/SYS_ChucVu
        [EnableQuery]
        public IQueryable<SYS_ChucVu> GetSYS_ChucVu()
        {
            return db.SYS_ChucVu;
        }

        // GET: odata/SYS_ChucVu(5)
        [EnableQuery]
        public SingleResult<SYS_ChucVu> GetSYS_ChucVu([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_ChucVu.Where(sYS_ChucVu => sYS_ChucVu.Id == key));
        }

        // PUT: odata/SYS_ChucVu(5)
        public IHttpActionResult Put([FromODataUri] Guid key, Delta<SYS_ChucVu> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_ChucVu sYS_ChucVu = db.SYS_ChucVu.Find(key);
            if (sYS_ChucVu == null)
            {
                return NotFound();
            }

            patch.Put(sYS_ChucVu);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_ChucVuExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_ChucVu);
        }

        // POST: odata/SYS_ChucVu
        public IHttpActionResult Post(SYS_ChucVu sYS_ChucVu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SYS_ChucVu.Add(sYS_ChucVu);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SYS_ChucVuExists(sYS_ChucVu.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sYS_ChucVu);
        }

        // PATCH: odata/SYS_ChucVu(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] Guid key, Delta<SYS_ChucVu> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SYS_ChucVu sYS_ChucVu = db.SYS_ChucVu.Find(key);
            if (sYS_ChucVu == null)
            {
                return NotFound();
            }

            patch.Patch(sYS_ChucVu);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SYS_ChucVuExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sYS_ChucVu);
        }

        // DELETE: odata/SYS_ChucVu(5)
        public IHttpActionResult Delete([FromODataUri] Guid key)
        {
            SYS_ChucVu sYS_ChucVu = db.SYS_ChucVu.Find(key);
            if (sYS_ChucVu == null)
            {
                return NotFound();
            }

            db.SYS_ChucVu.Remove(sYS_ChucVu);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SYS_ChucVu(5)/SYS_CV_NV
        [EnableQuery]
        public IQueryable<SYS_CV_NV> GetSYS_CV_NV([FromODataUri] Guid key)
        {
            return db.SYS_ChucVu.Where(m => m.Id == key).SelectMany(m => m.SYS_CV_NV);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SYS_ChucVuExists(Guid key)
        {
            return db.SYS_ChucVu.Count(e => e.Id == key) > 0;
        }
    }
}
