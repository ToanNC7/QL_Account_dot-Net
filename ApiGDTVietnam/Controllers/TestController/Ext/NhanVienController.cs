﻿using ApiGDTVietnam.DataProvider.EF;
using ApiGDTVietnam.Models;
using iTextSharp.text;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using RazorEngine;
using RazorEngine.Templating;
using System.Collections;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using ApiGDTVietnam.Controllers.Report.Service;

namespace ApiGDTVietnam.Controllers.TestController.Ext
{
    // [Authorize]
    [RoutePrefix("NhanVien")]
    public class NhanVienController : ApiController
    {
        private DatModelEntities1 db = new DatModelEntities1();
        private IDowloadService dowloadService;
        // Get thong tin cua 1 nhanh vien.
        [HttpGet]
        [Route("GetInfoNVByID")]
        public HttpResponseMessage GetNhanvien(Guid NhanVien_Id)
        {


            var _item = (from nv in db.SYS_Nhanvien
                         where nv.Id == NhanVien_Id
                         select new
                         {
                             nv.Id,
                             nv.Ten,
                             nv.Tuoi,
                             nv.GioiTinh,
                             nv.Id_Cty,
                             nv.Id_CV,
                             nv.Id_KN,

                         }).SingleOrDefault();
            if (_item != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _item);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Không tìm thấy nhân viên!");

        }


        [HttpGet]
        [Route("Download")]
        public HttpResponseMessage download(string mode, string filename)
        {
            var _item = db.SYS_Nhanvien.Select(s => new NhanVienmodel
            {
                Ten = s.Ten,
                Tuoi = s.Tuoi,
                GioiTinh = s.GioiTinh,
            }).ToList();
            string folder = "NhanVien";
            
            var html = dowloadService.RenderReportHtml(folder,_item);
            var _html = dowloadService.Download(html, null, PageSize.A4, mode, filename, false);
            if (_html != null)
            { return Request.CreateResponse(HttpStatusCode.OK, _html); }
            return Request.CreateResponse(HttpStatusCode.OK, "khong in duoc");

        }
        

        [HttpGet]
        [Route("GetAll")]
        public HttpResponseMessage GetAllNhanvien()
        {
            var _nv = db.SYS_Nhanvien;


            if (_nv != null)
            {
                var _model = _nv.Select(s => new
                {
                    TenNhanVien = s.Ten,
                    Tuoi = s.Tuoi,
                }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, _model);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Không tìm thấy nhân viên!");




        }



        [HttpPost]
        [Route("ADD")]
        public HttpResponseMessage AddNhanvien(NhanVienDto NhanVien)
        {
            var _cv = new SYS_ChucVu()
            {
                /*TenChucVU = NhanVien.TenChucVu*/

            };
            var _nv = new SYS_Nhanvien()
            {
                /*SYS_ChucVu = _cv,*/
                Ten = NhanVien.Ten,
                Tuoi = NhanVien.Tuoi,
                GioiTinh = NhanVien.GioiTinh
            };
            db.SYS_Nhanvien.Add(_nv);

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Thành công");
        }
        [HttpPost]
        [Route("ADD NV_CV")]
        public HttpResponseMessage AddNV_CV(NhanVienDto NhanVien)
        {

            var _nv = new SYS_Nhanvien()
            {

                Ten = NhanVien.Ten,
                Tuoi = NhanVien.Tuoi,
                GioiTinh = NhanVien.GioiTinh
            };
            var _CV = NhanVien.ChucVuDtos;

            _CV.ToList().ForEach(f =>
            {
                var _cv = new SYS_ChucVu()
                {

                    TenChucVU = f.TenChucVu
                };
                var _Cv_nv = new SYS_CV_NV()
                {
                    SYS_ChucVu = _cv,
                    SYS_Nhanvien = _nv

                };



                db.SYS_CV_NV.Add(_Cv_nv);
            });
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, "Thành công");
        }
        
       
        /*Code PDF*/
        [HttpGet]
        [Route("PDF2")]
        public IHttpActionResult ExampleTwo(Guid NhanVien_Id)
        {
            var stream = CreatePdf(NhanVien_Id);

            return ResponseMessage(new HttpResponseMessage
            {
                Content = new StreamContent(stream)
                {
                    Headers =
            {
                ContentType = new MediaTypeHeaderValue("application/pdf"),
                ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "myfile.pdf"
                }
            }
                },
                StatusCode = HttpStatusCode.OK
            });
        }
        public Stream CreatePdf(Guid NhanVien_Id)
        {
            /* using (var document = new Document(PageSize.A4, 50, 50, 25, 25))*/
            var _item = (from nv in db.SYS_Nhanvien
                         where nv.Id == NhanVien_Id
                         select new
                         {
                             nv.Id,
                             nv.Ten,
                             nv.Tuoi,
                             nv.GioiTinh,
                             nv.Id_Cty,
                             nv.Id_CV,
                             nv.Id_KN,

                         }).SingleOrDefault();
            string s = "";
            if (_item != null)
            {
                s = JsonConvert.SerializeObject(_item);
            }



            var output = new MemoryStream();
            var document = new Document(PageSize.A4, 50, 50, 25, 25);
            var writer = PdfWriter.GetInstance(document, output);
            writer.CloseStream = false;

            document.Open();
            /* document.Add(new Paragraph (s));*/

            Paragraph para = new Paragraph("Header", new Font(Font.HELVETICA, 22));
            para.Alignment = Element.ALIGN_CENTER;
            document.Add(para);
            document.Add(new Paragraph(s));
            document.NewPage();


            document.Close();

            output.Seek(0, SeekOrigin.Begin);

            return output;

        }
    }
}