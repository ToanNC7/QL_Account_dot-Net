﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ApiGDTVietnam.DataProvider.EF;
using ApiGDTVietnam.Ultility.BaseControllers;

namespace ApiGDTVietnam.Controllers.TestController
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ApiGDTVietnam.DataProvider.EF;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Role>("Roles");
    builder.EntitySet<TaiKhoan>("TaiKhoans"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RolesController : BaseOdataWithDeleteController<Guid, SYS_ROLETS, SeeModels>
    {
        public RolesController() { db = new SeeModels(); }

        // GET: odata/Roles
        [EnableQuery]
        public IQueryable<SYS_ROLETS> GetRoles()
        {
            return db.SYS_ROLETS;
        }

        // GET: odata/Roles(5)
        [EnableQuery]
        public SingleResult<SYS_ROLETS> GetRole([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.SYS_ROLETS.Where(role => role.Id == key));
        }


    }
}
