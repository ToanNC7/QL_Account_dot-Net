﻿using iTextSharp.text;
using System.Net.Http;
using System.Xml.Linq;


namespace ApiGDTVietnam.Controllers.Report.Service
{
    public interface IDowloadService
    {
        string RenderReportHtml(string folder, object model);
        HttpResponseMessage Download(string html, XDocument xml, Rectangle pageZize, string mode, string filename, bool logo);
    }
}